package de.bennypi.reviewboardtestapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewboardTestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReviewboardTestAppApplication.class, args);
	}
}
